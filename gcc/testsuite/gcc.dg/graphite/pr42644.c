/* Testcase extracted from test 183.equake in SPEC CPU2000.  */
#include <stdio.h>

double Ke[2], ds[2];

void foo(double Ke[2], int i, double ds[],  int column)
{ 
    double tt, ts;
    unsigned long long j; 
    
    for (j = 0; j < 2; j++) { 
	++column;
          ts = ds[i];
          if (i == j)
            tt = 123;
	  else 
            tt = 0;
          Ke[column] = Ke[column] + ts + tt;
        }
}


int
main ()
{ 
  int i, j;

  ds[0] = 1.0;
  ds[1] = 1.0;

  foo(Ke, 0, ds, -1);

  for (i = 0; i < 1; i++)
  { 
    for (j = 0; j < 2; j++) 
      printf ("%d ",  (int) Ke[j]);
      printf("# ");
    }

    printf("\n");

  return 0;
}
