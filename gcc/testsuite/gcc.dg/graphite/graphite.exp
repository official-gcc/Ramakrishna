#   Copyright (C) 2006, 2007, 2008 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with GCC; see the file COPYING3.  If not see
# <http://www.gnu.org/licenses/>.

# GCC testsuite that uses the `dg.exp' driver.

# Load support procs.
load_lib gcc-dg.exp

if ![check_effective_target_fgraphite] {
  return
}

# Remove VALUE from LIST_VARIABLE.
proc lremove {list_variable value} {
    upvar 1 $list_variable var
    set idx [lsearch -exact $var $value]
    set var [lreplace $var $idx $idx]
}

# The default action for a test is 'compile'.  Save current default.
global dg-do-what-default
set save-dg-do-what-default ${dg-do-what-default}
set dg-do-what-default compile

# Initialize `dg'.
dg-init

# Main loop.

set wait_to_run_files [lsort [glob -nocomplain $srcdir/$subdir/*.c ] ]

# Flags using for block-* files.
set DEFAULT_FLAGS_GRAPHITE_BLOCK "-O2 -floop-block -fno-loop-strip-mine \
 -fno-loop-interchange -fdump-tree-graphite-all"
set block_files [lsort [glob -nocomplain $srcdir/$subdir/block-*.c ] ]
dg-runtest $block_files "" $DEFAULT_FLAGS_GRAPHITE_BLOCK
foreach block_file $block_files {lremove wait_to_run_files $block_file}

# Flags using for id-* files.
set DEFAULT_FLAGS_GRAPHITE_IDENTITY "-O2 -fgraphite-identity"
set id_files [lsort [glob -nocomplain $srcdir/$subdir/id-*.c ] ]
dg-runtest $id_files "" $DEFAULT_FLAGS_GRAPHITE_IDENTITY
foreach id_file $id_files {lremove wait_to_run_files $id_file}

# Flags using for interchange-* files.
set DEFAULT_FLAGS_GRAPHITE_INTERCHANGE "-O2 -fdump-tree-graphite-all \
 -floop-interchange -fno-loop-block -fno-loop-strip-mine -ffast-math"
set interchange_files [lsort [glob -nocomplain $srcdir/$subdir/interchange-*.c ] ]
dg-runtest $interchange_files "" $DEFAULT_FLAGS_GRAPHITE_INTERCHANGE
foreach interchange_file $interchange_files {lremove wait_to_run_files $interchange_file}

# Flags using for scop-* files.
set DEFAULT_FLAGS_GRAPHITE_SCOP "-O2 -fgraphite -fdump-tree-graphite-all"
set scop_files [lsort [glob -nocomplain $srcdir/$subdir/scop-*.c ] ]
dg-runtest $scop_files "" $DEFAULT_FLAGS_GRAPHITE_SCOP
foreach scop_file $scop_files {lremove wait_to_run_files $scop_file}


# Schedule now the tests to be run.
set dg-do-what-default run

# Flags using for run-id-* files.
set DEFAULT_FLAGS_RUN_ID "-O2 -fgraphite-identity"
set run_id_files [lsort [glob -nocomplain $srcdir/$subdir/run-id-*.c ] ]
dg-runtest $run_id_files "" $DEFAULT_FLAGS_RUN_ID
foreach run_id_file $run_id_files {lremove wait_to_run_files $run_id_file}


# The default action for the rest of the files is 'compile'.
set dg-do-what-default compile

# Flags using for other files.
set DEFAULT_FLAGS_GRAPHITE "-ansi -pedantic-errors"
dg-runtest $wait_to_run_files "" $DEFAULT_FLAGS_GRAPHITE

# Clean up.
set dg-do-what-default ${save-dg-do-what-default}

# All done.
dg-finish
